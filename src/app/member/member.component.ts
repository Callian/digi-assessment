import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent {

  points: number;

  constructor() {
    this.points = Math.floor(Math.random() * 10000) + 1000;
  }

}
