import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  carouselData: any;
  carouselText: string;

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getDigiData().subscribe(
      data => {
        this.carouselData = data.Carousel;
      }
    );
  }

  addSlideToCarousel() {

    if (this.carouselText !== undefined && this.carouselText.trim() !== '') {
      if (this.carouselData) {
        this.carouselData.push(this.carouselText);
        this.carouselText = '';
      }
    }

  }

}
