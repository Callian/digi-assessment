import { Component, OnInit } from '@angular/core';
import { DataService } from '../../app/services/data.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public digiData = [];

  constructor(private data: DataService) { }

  ngOnInit() {
    this.data.getDigiData().subscribe(
      data => this.digiData = data
    );
  }

}
