import { NgModule } from '@angular/core';

import {
    MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule,
    MatInputModule, MatFormFieldModule, MatCardModule, MatProgressSpinnerModule
} from '@angular/material';

@NgModule({
    imports: [MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatInputModule
        , MatFormFieldModule, MatCardModule, MatProgressSpinnerModule],
    exports: [MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatInputModule
        , MatFormFieldModule, MatCardModule, MatProgressSpinnerModule],
})
export class MaterialModule { }
