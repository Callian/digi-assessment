import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent {

  showLoader: boolean;
  userLoginForm: FormGroup;
  userLoginData: {
    emailAddress: string,
    password: string
  };

  constructor(fb: FormBuilder, private userService: UserService, private router: Router, private toastr: ToastrService) {
    this.userLoginForm = fb.group({
      emailAddress: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: ['', Validators.required]
    });
  }

  submitLoginForm() {
    this.userLoginData = {
      emailAddress: this.userLoginForm.value.emailAddress,
      password: this.userLoginForm.value.password
    };

    this.showLoader = true;

    this.userService.loginUser(this.userLoginData).subscribe(
      data => {

        this.showLoader = false;

        if (data === true) {
          this.toastr.success('Login Successfully', 'Success');
          this.router.navigate(['member']);
        } else {
          this.toastr.error('Invalid Email Address or Password', 'Login Error', {
            timeOut: 3000,
          });
        }
      }
    );
  }
}
