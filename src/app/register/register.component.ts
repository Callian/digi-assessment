import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent {

  showLoader: boolean;
  userRegistrationForm: FormGroup;
  userRegistrationData: {
    username: string,
    emailAddress: string,
    password: string
  };

  constructor(fb: FormBuilder, public userService: UserService, private router: Router, private toastr: ToastrService) {
    this.userRegistrationForm = fb.group({
      username: ['', Validators.required],
      emailAddress: new FormControl('', Validators.compose([
        Validators.required,
        Validators.email
      ])),
      password: ['', Validators.required]
    });
  }

  submitRegistrationForm() {

    this.userRegistrationData = {
      username: this.userRegistrationForm.value.username,
      emailAddress: this.userRegistrationForm.value.emailAddress,
      password: this.userRegistrationForm.value.password
    };

    this.showLoader = true;

    this.userService.registerUser(this.userRegistrationData).subscribe(
      data => {

        this.showLoader = false;

        if (data === true) {
          this.toastr.success('Registerd Successfully', 'Success');
          this.router.navigate(['login']);
        } else {
          this.toastr.error('Something went wrong', 'Registration Error', {
            timeOut: 3000,
          });
        }
      }
    );
  }

}
