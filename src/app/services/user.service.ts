import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url: string;

  constructor(private http: HttpClient) { }

  registerUser(userRegistration): Observable<any> {
    this.url = 'http://victorycasino.azurewebsites.net/users/registerUser';
    return this.http.post(this.url, userRegistration);
  }

  loginUser(userLogin): Observable<any> {
    this.url = 'http://victorycasino.azurewebsites.net/users/loginUser';
    return this.http.post(this.url, userLogin);
  }
}
